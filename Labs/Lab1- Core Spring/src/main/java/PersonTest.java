import com.conygre.test.pets.Person;
import com.conygre.test.pets.PersonConfiguration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class PersonTest {
    public static void main(String[] args) {
        ApplicationContext context = new  AnnotationConfigApplicationContext(PersonConfiguration.class);
        context.getBean(Person.class).getPet().feed();
    
    }
}
