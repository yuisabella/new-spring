package com.conygre.spring.data;

import com.conygre.spring.entities.CompactDisc;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CompactDiscRepository extends MongoRepository<CompactDisc, ObjectId>{
    
}