package com.conygre.spring.controller;

import com.conygre.spring.entities.CompactDisc;
import com.conygre.spring.service.CompactDiscService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/compactdiscs")
public class CompactDiscController {
    @Autowired
    private CompactDiscService service;
    
    @RequestMapping(method = RequestMethod.GET)
    public Iterable<CompactDisc> getAllDiscs(){
        return service.getCatalog();
    }
    @RequestMapping(method = RequestMethod.POST)
    public void addCd(@RequestBody CompactDisc disc) {
	    service.addToCatalog(disc);
    }
}